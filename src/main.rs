use clap::Parser;
use rand::{rngs::StdRng, Rng, RngCore, SeedableRng};

fn measure(nb_measurements: usize, rng: &(impl RngCore + Clone)) {
    let mut rng = rng.clone();
    for _ in 0..nb_measurements {
        println!("    {}", rng.gen::<u128>());
    }
}

fn run<Fun: Fn(usize) -> bool>(
    max_t: usize,
    nb_measurements: usize,
    measurement_schedule: Fun,
    rng: &mut (impl RngCore + Clone),
) {
    println!("0: {}", rng.gen::<u128>());
    for t in 1..=max_t {
        if measurement_schedule(t) {
            measure(nb_measurements, rng);
        }

        println!("{}: {}", t, rng.gen::<u128>());
    }
}

fn parse_hex_string(s: &str) -> Result<[u8; 32], String> {
    if s.len() != 64 {
        return Err("Input string must be exactly 64 characters long".to_string());
    }

    match hex::decode(s) {
        // `bytes` will be a `Vec<u8>` of size `32`, so it's safe to `unwrap`
        // the conversion to `[u8: 32]`
        Ok(bytes) => Ok(bytes.try_into().unwrap()),
        Err(e) => Err(format!("Failed to decode hex string: {}", e)),
    }
}

#[derive(Parser)]
struct Cli {
    #[arg(short)]
    t: usize,

    #[arg(long)]
    nb_measurements: usize,

    #[arg(long)]
    measurement_schedule: usize,
    #[arg(long)]
    measurement_schedule_start: usize,

    #[arg(long, value_parser = parse_hex_string)]
    prng_seed: [u8; 32],
}

fn main() {
    let cli = Cli::parse();

    let mut rng = StdRng::from_seed(cli.prng_seed);

    let measurement_schedule = |t| {
        t >= cli.measurement_schedule_start
            && (t - cli.measurement_schedule_start) % cli.measurement_schedule == 0
    };

    run(cli.t, cli.nb_measurements, measurement_schedule, &mut rng);
}
